# 模拟各种向接口发送的请求
import base64
import requests.sessions
from common.read_ini import ReadIni


class RequestMethod:
    def __init__(self):
        # 获取请求的ur，定义登录数据用requests的session对象自动关联请求头部信息
        login_path = ReadIni().get_host("test_bpm") + "/auth"
        login_data = {"username": "admin", "password": base64.b64encode("123456".encode("utf-8")).decode("utf-8")}
        self.bpm_session = requests.sessions.Session()
        result = self.bpm_session.request(method="post", url=login_path, json=login_data)
        self.bpm_session.headers["Authorization"] = "Bearer " + result.json().get("token")

    def request_all(self, method, url, mime=None, data=None):
        if mime == "json" or mime == "application/json":
            return self.bpm_session.request(method=method, url=url, json=data)
        elif mime == "form-data" or mime == "multipart/form-data":
            return self.bpm_session.request(method=method, url=url, files=data)
        elif mime == "x-www-form-urlencoded" or mime == "application/x-www-form-urlencoded":
            return self.bpm_session.request(method=method, url=url, data=data)
        elif mime == "text/plain" or mime == "text":
            return self.bpm_session.request(method=method, url=url, data=data)
        elif mime == "query" or mime == "params":
            return self.bpm_session.request(method=method, url=url, params=data)
        elif mime == "application/json|query" or mime == "json|query" or mime == "query|json":
            return self.bpm_session.request(method=method, url=url, params=data["query"], json=data["body"])
        elif mime is None:
            return self.bpm_session.request(method=method, url=url)
        else:
            raise TypeError("传入的媒体类型错误")


if __name__ == '__main__':
    rm=RequestMethod()