# 测试bpm系统,如果存在前置条件,例如对数据库进行操作，需要先运行SQL语句
# 测试更新文件是否实现自动化集成
import logging

import allure
import pytest

from common.read_excel import ReadExcel


class Test_BPM:
    @allure.epic("BPM系统")
    @pytest.mark.parametrize(
        "module, interface, title, level, req_method, req_url, mime, case_data, export_data,sql_type, sql, update_key",
        ReadExcel().get_data())
    def test_bpm(self, db_fix, req_fix, module, interface, title, level, req_method, req_url, mime, case_data,
                 export_data, sql_type, sql, update_key):

        # 模块名称，接口名称，用例标题，等级
        allure.dynamic.feature(module)
        allure.dynamic.story(interface)
        allure.dynamic.title(title)
        allure.dynamic.severity(level)

        print("casedata:", module, interface, title, level, req_method, req_url, mime, case_data, export_data, sql_type,
              sql, update_key)

        if sql_type == "delete":
            db_fix.delete(sql)
        elif sql_type == "select":
            result = db_fix.select(sql)
            case_data[update_key] = result
        elif sql_type == "select|delete" or sql_type == "delete|select":
            db_fix.delete(sql["delete"])
            result = db_fix.select(sql["select"])
            case_data[update_key] = result
        print(mime,case_data)
        result = req_fix.request_all(method=req_method, url=req_url, mime=mime, data=case_data)

        try:
            for key in export_data:
                assert export_data[key] == result.json().get(key)
        except:
            logging.error(
                f"模块名称:{module},接口名称:{interface}，用例标题:{title}，用例等级:{level},请求方法：{req_method},请求路径：{req_url}，媒体类型：{mime}，用例数据：{case_data}，期望结果：{export_data}")
            raise AssertionError("断言失败")
        else:
            print("断言成功")

if __name__ == '__main__':
    pytest.main()
