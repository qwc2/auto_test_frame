# 自定义固件和脚手架
import pytest

from common.db import DB
from request_method.request_method import RequestMethod


@pytest.fixture(scope="session")
def db_fix():
    db = DB()
    yield db


@pytest.fixture(scope="session")
def req_fix():
    rm = RequestMethod()
    yield rm

#解决测试用例乱码问题
def pytest_collection_modifyitems(items):
    # item表示每个测试用例，解决用例名称中文显示问题
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode-escape")
        item._nodeid = item._nodeid.encode("utf-8").decode("unicode-escape")