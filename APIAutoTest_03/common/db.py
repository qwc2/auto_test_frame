# 连接mysql数据库，对数据库进行操作
import pymysql

from common.read_ini import ReadIni


class DB:
    def __init__(self):
        # 从read_int中读取出连接所需信息，用pymysql对象创建连接对象，再用cursor创建游标对象
        ri = ReadIni()
        host = ri.get_pdbc("host")
        port = int(ri.get_pdbc("port"))
        user = ri.get_pdbc("user")
        pwd = ri.get_pdbc("pwd")
        database = ri.get_pdbc("database")
        self.conn = pymysql.connect(host=host, port=port, user=user, password=pwd, database=database, charset="utf8")
        self.cursor = self.conn.cursor()

    def select(self, sql_sentence):
        self.cursor.execute(sql_sentence)
        result = self.cursor.fetchall()
        if result:
            return result[0][0]

    def delete(self, sql_sentence):
        result=self.cursor.execute(sql_sentence)
        self.conn.commit()
        return result

    def __del__(self):
        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    db=DB()
    print(db.select("SELECT ID_ FROM uc_demension WHERE `CODE_`=\"python_dem\";"))
    print(db.delete("DELETE FROM uc_org WHERE `CODE_`=\"python_org\";"))
