# 读取excel文件中的数据
import openpyxl

from common.read_ini import ReadIni
from common.read_json import read_json
from config_data.setting import *


class ReadExcel:
    def __init__(self):
        # 加载excel文件路径，用openpyxl加载工作薄，将json的值取出来存放在python对象中，获取工作表名，加载工作表
        ri = ReadIni()
        excel_path = ri.get_file("excel")
        case_path = ri.get_file("case")
        export_path = ri.get_file("export")
        sql_path = ri.get_file("sql")

        self.case_data = read_json(case_path)
        self.export_data = read_json(export_path)
        self.sql_data = read_json(sql_path)

        self.wb = openpyxl.load_workbook(excel_path)
        self.ws = self.wb[ri.get_table_name("table")]

    def __get_cell_data(self, column: str, row: int) -> str:
        value = self.ws[column + str(row)].value
        if value is None:
            return None
        elif value.strip():
            return value.strip()

    def get_module_name(self, row):
        return self.__get_cell_data(MODULE, row)

    def get_interface_name(self, row):
        return self.__get_cell_data(API, row)

    def get_title(self, row):
        return self.__get_cell_data(TITLE, row)

    def get_case_level(self, row):
        return self.__get_cell_data(LEVEL, row)

    def get_request_method(self, row):
        return self.__get_cell_data(REQUESTMETHOD, row)

    def get_url(self, row):
        return ReadIni().get_host("test_bpm") + self.__get_cell_data(URL, row)

    def get_mime(self, row):
        value = self.__get_cell_data(MIME, row)
        if value:
            return value.lower()

    def get_case_data(self, row):
        # 获取用例数据的key，模块名称，接口名称，再根据key到case_data中取出数据
        key = self.__get_cell_data(CASEDATA, row)
        if key:
            module = self.get_module_name(row)
            interface = self.get_interface_name(row)
            return self.case_data[module][interface][key]

    def get_export_data(self, row):
        # 获取期望数据的key，模块名称，接口名称，再根据key到export_data中取出数据
        key = self.__get_cell_data(EXPORT, row)
        module = self.get_module_name(row)
        interface = self.get_interface_name(row)
        return self.export_data[module][interface][key]

    def get_sql_data(self, row):
        # 获取sql数据的key，模块名称，接口名称，再根据key到export_data中取出数据,注意可能数据中有些没有SQL语句
        key = self.__get_cell_data(SQL, row)
        if key:
            module = self.get_module_name(row)
            interface = self.get_interface_name(row)
            return self.sql_data[module][interface][key]

    def get_sql_type(self, row):
        value = self.__get_cell_data(SQLTYPE, row)
        if value:
            return value.lower()

    def get_update_key(self, row):
        return self.__get_cell_data(UPDATAKEY, row)

    # 获取excel文件中的数据
    def get_data(self):
        excel_list = []
        for i in range(2, self.ws.max_row + 1):
            module = self.get_module_name(i)
            interface = self.get_interface_name(i)
            title = self.get_title(i)
            level = self.get_case_level(i)
            req_method = self.get_request_method(i)
            req_url = self.get_url(i)
            mime = self.get_mime(i)
            case_data = self.get_case_data(i)
            export_data = self.get_export_data(i)
            sql_type = self.get_sql_type(i)
            sql = self.get_sql_data(i)
            update_key = self.get_update_key(i)
            excel_list.append([module, interface, title, level, req_method, req_url, mime, case_data, export_data,
                               sql_type, sql, update_key])
        else:
            return excel_list


if __name__ == '__main__':
    re = ReadExcel()
    print(re.get_data())
