# 读取json文件的通用方法
import json


def read_json(filename):
    with open(filename, mode="r", encoding="utf-8") as fp:
        return json.load(fp)


if __name__ == '__main__':
    print(read_json(r"D:\PythonProject\auto_test_frame\APIAutoTest_03\config_data\case_data.json"))
