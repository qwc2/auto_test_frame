# 读取config.ini文件
import configparser
import os


class ReadIni:
    def __init__(self):
        # 获取config.ini的路径，调用configparser对象读取文件，将该对象设置为对象属性
        self.dirname_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "config_data")
        config_path = os.path.join(self.dirname_path, "config.ini")
        self.conf = configparser.ConfigParser()
        self.conf.read(config_path, encoding="utf-8")

    # 读取file节点下的数据
    def get_file(self, key):
        return os.path.join(self.dirname_path, self.conf.get("file", key))

    # 获取table_name节点下的数据
    def get_table_name(self, key):
        return self.conf.get("table_name", key)

    # 读取host节点下的数据，存放测试系统的url
    def get_host(self, key):
        return self.conf.get("host", key)

    # 获取pdbc节点下的连接数据库的信息
    def get_pdbc(self, key):
        return self.conf.get("pdbc", key)


if __name__ == '__main__':
    ri = ReadIni()
    print(ri.get_file("case"))
    print(ri.get_table_name("table"))
    print(ri.get_host("test_bpm"))
    print(ri.get_pdbc("pwd"))
